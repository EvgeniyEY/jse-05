package ru.ermolaev.tm;

import ru.ermolaev.tm.constant.ITerminalConst;

public class Application implements ITerminalConst {

    public static void main(final String[] args) {
        System.out.println("Welcome to task manager");
        showInfo(args);
    }

    private static void showInfo(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0].toUpperCase();
        if (arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.HELP:
                showHelp();
                break;
            case ITerminalConst.ABOUT:
                showAbout();
                break;
            case ITerminalConst.VERSION:
                showVersion();
                break;
            default:
                System.out.println("Invalid parameter");
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(ITerminalConst.ABOUT + " - Show developer info.");
        System.out.println(ITerminalConst.VERSION + " - Show version info.");
        System.out.println(ITerminalConst.HELP + " - Display terminal commands.");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

}
